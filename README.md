# Dynamic attributes for Model

Manage dynamic attributes for your model in Rails app.

Use for:
* Useful for managing a large number of properties.
* Can be used to have localized fields or versioned fields. 
* Store values for multi value fields. For example, for many-to-many relationships of the model with other models.
It makes it easy to store data for localized fields. 


# Quick start

# Usage



## Localized attributes



# Storage

Properties data are stored in a separate table.

For a given model Product it will have table
product_fields - all fields with metadata
product_field_values - field values for product


# Examples