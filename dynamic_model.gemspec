$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require_relative "lib/dynamic_model/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "dynamic_model"
  s.version     = DynamicModel::VERSION
  s.authors     = ["Max Ivak"]
  s.email       = ["maxivak@gmail.com"]
  s.homepage    = "https://gitlab.com/maxivak/dynamic_model"
  s.summary     = "Add dynamic attributes to your model"
  s.description = "Useful for managing a large number of properties."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  # s.add_dependency 'zeitwerk'
  s.add_dependency 'rails'
end
