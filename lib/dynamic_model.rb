require_relative "dynamic_model/field_type"
require_relative "dynamic_model/entity_field"
require_relative "dynamic_model/entity_field_value"
require_relative "dynamic_model/field_manager"
require_relative "dynamic_model/properties_data"
require_relative "dynamic_model/model"


# require "zeitwerk"
# loader = Zeitwerk::Loader.for_gem
#
#
# # loader.push_dir File.expand_path("../../../lib", __FILE__)
# loader.push_dir File.expand_path("../api/", __FILE__)
#
# loader.ignore("#{__dir__}/api/engine")
#
# loader.setup


module DynamicModel

end
