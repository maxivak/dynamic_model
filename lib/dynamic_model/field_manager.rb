module DynamicModel
  class FieldManager
    attr_accessor :options

    def initialize(options={})
      @options = options
    end

    def field_class
      @field_class ||= @options[:field_class]
    end

    def add_field(name, type, opts={})
      is_multivalue = opts[:multivalue] || false
      is_versioned = opts[:versioned] || false
      field = field_class.new(
          name: name,
          type_id: FieldType.get_id_by_name(type),
          multivalue: is_multivalue,
          versioned: is_versioned,
      )

      res = field.save

      res
    end

    def get_field_by_name(field_name)
      field_class.where(name: field_name).first
    end

  end
end