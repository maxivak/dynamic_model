module DynamicModel
  class PropertiesData
    attr_accessor :options

    # cache
    @properties_cache = {}

    def initialize(entity_record, options={})
      @entity_record = entity_record
      @options = options

      @properties_cache = {}
    end

    def properties_cache
      return @properties_cache unless @properties_cache.nil?

      @properties_cache = {}
    end

    def entity_record
      @entity_record
    end

    def field_class
      @field_class ||= @options[:field_class]
    end

    def field_value_class
      @field_value_class ||= @options[:field_value_class]
    end

    def get(field_name, def_value=nil, opts={})
      version = opts[:version] || nil

      # cache
      v_cache, cache_exist = cache_get_value field_name, version
      return v_cache if cache_exist

      #
      if field_name.is_a?(String)
        field = get_field_by_name field_name
      else
        field = field_name
      end

      return nil if field.nil?

      if field.multivalue
        value_records = get_value_records field.id, version
        v = value_records.map{|r| get_value(r)}
      else
        value_record = get_value_record field.id, version
        return nil if value_record.nil?

        v = get_value value_record
      end

      cache_set_value field_name, version, v

      v
    end

    def get_value_record(field_id, version=nil)
      field_value_class.where(entity_id: entity_record.id, field_id: field_id, version: version).first
    end

    def get_value_records(field_id, version=nil)
      field_value_class.where(entity_id: entity_record.id, field_id: field_id, version: version).all.to_a
    end

    def set(field_name, value, opts={})
      field = get_field_by_name field_name
      raise 'Field not found' if field.nil?

      if field.multivalue
        set_multivalue_field_value field, value, opts
      else
        set_field_value field, value, opts
      end
    end

    def get_value(value_record)
      type_id = value_record.field.type_id
      if [FieldType::INT, FieldType::REFERENCE].include? type_id
        return value_record.value.to_i
      elsif [FieldType::TEXT].include? type_id
        return value_record.value_text
      end

      value_record.value
    end

    private

    def set_field_value(field, value, opts={})
      version = opts[:version] || nil
      value_record = get_value_record field.id, version

      if value_record.nil?
        value_record = field_value_class.new(entity_id: entity_record.id, field_id: field.id, version: version)
      end

      # set value
      if [FieldType::TEXT].include? field.type_id
        value_record.value_text = value
      else
        value_record.value = value
      end

      value_record.save
    end

    def set_multivalue_field_value(field, values, opts={})
      version = opts[:version] || nil

      values = values.map{|r| r.to_s}.reject(&:blank?)

      # existing values
      existing_rows = field_value_class.where(entity_id: entity_record.id, field_id: field.id, version: version).all.to_a
      existing_values = existing_rows.map{|r| r.value}

      delete_values = existing_values - values
      new_values = values - existing_values

      ActiveRecord::Base.transaction do
        # delete
        if delete_values.length > 0
          field_value_class.where(entity_id: entity_record.id, field_id: field.id, value: delete_values).delete_all
        end

        # insert
        new_values.each do |new_value|
          next if new_value.blank?

          new_record = field_value_class.new(entity_id: entity_record.id, field_id: field.id, value: new_value)
          new_record.save!
        end
      end
    end

    def get_field_by_name(field_name)
      field_class.where(name: field_name).first
    end

    def cache_get_value(field_name, version)
      version_name = version || 'default'

      exist = @properties_cache.has_key?(field_name) && @properties_cache[field_name].has_key?(version_name)
      return [nil, exist] if !exist

      v = @properties_cache.dig(field_name, version_name)
      [v, true]
    end

    def cache_set_value(field_name, version, v)
      version_name = version || 'default'
      @properties_cache[field_name] = {} unless @properties_cache.has_key?(field_name)
      @properties_cache[field_name][version_name] = v
    end

  end
end