module DynamicModel
  module Model
    extend ActiveSupport::Concern

    included do
      def properties
        @properties ||= DynamicModel::PropertiesData.new(self, self.class.dynamic_model_settings)
      end

      def self.properties
        FieldManager.new(dynamic_model_settings)
      end
    end

  end
end