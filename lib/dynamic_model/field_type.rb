module DynamicModel
  class FieldType
    INT = 1
    STRING = 2
    DATETIME = 3
    TEXT = 4
    ENUM = 5
    DATE = 6
    TIME = 7
    BOOL = 8
    IMAGE = 9
    REFERENCE = 10
    MONEY = 11

    TYPES = {
        :int => INT,
        :string => STRING,
        :text => TEXT,
        :bool => BOOL,
        :reference => REFERENCE,
        :money => MONEY,
    }

    def self.get_id_by_name(name)
      TYPES[name.to_sym]
    end

    def self.get_name_by_id(id)
      TYPES.invert[id].to_s
    end
  end
end

