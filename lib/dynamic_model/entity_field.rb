module DynamicModel
  module EntityField
    extend ActiveSupport::Concern

    included do

      def self.get_by_name(name)
        row = where(name: name).first
        row
      end

      def self.get_id_by_name(name)
        row = get_by_name name
        return nil if row.nil?

        row.id
      end

    end

  end
end





