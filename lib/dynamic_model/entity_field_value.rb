module DynamicModel
  module EntityFieldValue
    extend ActiveSupport::Concern

    included do
      belongs_to :entity, class_name: dynamic_model_settings[:entity_class].name, foreign_key: :entity_id
      belongs_to :field, class_name: dynamic_model_settings[:field_class].name, foreign_key: :field_id
    end

  end
end

